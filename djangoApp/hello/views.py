from django.shortcuts import render
from . import forms

def index(request):
    classform = forms.classform()

    context = {
        'heading':'Home',
        'classform': classform
    }
    if request.method == 'POST':
      
        print("Ini adalah method POST")
        context = {
            'nama': request.POST['nama'], 
            'alamat': request.POST['alamat'],
            'namac': request.POST['namac'], 
            'alamatc': request.POST['alamatc'],
        }
    else:
        print("Ini adalah method GET")
    return render(request, 'hello/index.html', context)


       
