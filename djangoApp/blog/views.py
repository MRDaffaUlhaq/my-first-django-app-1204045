from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .models import Post
from . import formsArticle

def index(request):
    
    return render(request, 'blog/index.html')

def post(request):
    classform = formsArticle.classform(request.POST or None)

    if request.method == 'POST':
        if classform.is_valid():
            classform.save()
            return HttpResponseRedirect('/blog/post')

    db = Post.objects.all()
    context = {
        'title':'Blog',
        'heading':'Add Article Here',
        'subheading':'Posts',
        'classform': classform,
        'post': db,
    }
    return render(request, 'blog/post.html', context)

def delete(request, id):
    Post.objects.filter(id=id).delete()
    return HttpResponseRedirect('/blog/post')

def update(request, id):
    updateCurrentArt = Post.objects.get(id=id)

    data = {
        'title': updateCurrentArt.title,
        'body': updateCurrentArt.body
    }

    classform = formsArticle.classform(request.POST or None, initial=data, instance=updateCurrentArt)

    if request.method == 'POST':
        if classform.is_valid():
            classform.save()
            return HttpResponseRedirect('/blog/post')

    context = {
        'heading':'Update Article',
        'classform': classform,      
    }

    return render(request, 'blog/post.html', context)

def recent(request):
    return HttpResponse("Hello Daffa!")

def articles(request,year):
    year=year

    str = year

    return HttpResponse(year)



