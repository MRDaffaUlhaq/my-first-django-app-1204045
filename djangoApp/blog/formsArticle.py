from django import forms
from django.forms import Textarea
from blog.models import Post

class classform(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            'title',
            'body',
        ]
    title = forms.CharField()
    body = forms.CharField(widget=Textarea)